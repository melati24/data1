import { Component } from '@angular/core';
import { NavController, ItemSliding} from 'ionic-angular';
import { GESTURE_PRIORITY_SLIDING_ITEM } from 'ionic-angular/umd/gestures/gesture-controller';

@Component({
  selector: 'tasklist',
  templateUrl: 'tasklist.html'
})
export class TaskList {

  tasks : Array<any> = [];

  constructor(public navCtrl: NavController) {

    this.tasks = [
      {title:'Nasi Goreng', status:'open'},
      {title:'Nasi Padang', status:'open'},
      {title:'Nasi Kuning', status:'open'},
      {title:'Nasi Kucing', status:'open'},
      {title:'Nasi Bungkus', status:'open'}
    ]

  }
  addItem(){
    let theNewTask: string = prompt ("New Task");
    if (theNewTask!= '') {
      this.tasks.push({title: theNewTask, status: 'open'})
    }
  }

  markAsDone(slidingItem:ItemSliding, task:any ) {
    task.status = 'done';
    slidingItem.close();
  }

  removeTask(task) {
    task.status='remove';
    let index = this.tasks.indexOf(task);
    if (index > -1){
      this.tasks.splice(index,1);
    }
  }
  

}
